<?php
require '../../../zb_system/function/c_system_base.php';
require '../../../zb_system/function/c_system_admin.php';

$zbp->Load();

if (!$zbp->CheckRights('root')) {$zbp->ShowError(6);die();}
if (!$zbp->CheckPlugin('mip_push')) {$zbp->ShowError(48);die();}

if (isset($_POST['token'])) {
    $zbp->Config('mip_push')->token = $_POST['token'];
	$zbp->Config('mip_push')->authkey = $_POST['authkey'];
    $zbp->SaveConfig('mip_push');
    $zbp->SetHint('good', '参数已保存');
}

require $blogpath .'zb_system/admin/admin_header.php';
require $blogpath .'zb_system/admin/admin_top.php';

?>

<div id="divMain">
    <div class="divHeader">百度MIP数据提交</div>
    <div class="SubMenu">
        <a href="<?php echo $zbp->host; ?>zb_users/plugin/AppCentre/main.php?id=1520" target="_blank"><span class="m-left ">参数说明</span></a>
		<a href="https://www.songhaifeng.com/zblog-plugin/136.html" target="_blank"><span class="m-left ">技术支持</span></a>
	</div>
    <div id="divMain2">
        <form id="form1" name="form1" method="post">
            <table width="100%" style="padding:0;margin:0;" cellspacing="0" cellpadding="0" class="tableBorder">
                <tr>
                    <td width="20%" height="50px" align="center">域名</td>
                    <td width="40%" align="center"><?php echo $zbp->host; ?></td>
                </tr>
                <tr>
                    <td width="20%" height="50px" align="center">token</td>
                    <td width="40%" align="center">
                        <input name="token" id="token" type="text" value="<?php echo $zbp->Config('mip_push')->token; ?>" style="width:90%;height:30px;letter-spacing:1px; " required="required" />
                    </td>
                </tr>
				<tr>
                    <td width="20%" height="50px" align="center">Authkey</td>
                    <td width="40%" align="center">
                        <input name="authkey" id="authkey" type="text" value="<?php echo $zbp->Config('mip_push')->authkey; ?>" style="width:90%;height:30px;letter-spacing:1px; " required="required" />
                    </td>
                </tr>
                <tr>
                    <td colspan="3" width="100%" height="50px" align="center">
                        <input name="" type="Submit" class="button" style="width: 88%;" value="保存" />
                    </td>
                </tr>
            </table>
        </form>
        <script type="text/javascript">
            ActiveLeftMenu("aPluginMng");
            AddHeaderIcon("<?php echo $bloghost . 'zb_users/plugin/mip_push/logo.png'; ?>");
        </script>
    </div>
</div>

<?php
    require $blogpath .'zb_system/admin/admin_footer.php';
    RunTime();
?>