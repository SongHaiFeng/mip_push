<?php

RegisterPlugin("mip_push","ActivePlugin_mip_push");

function ActivePlugin_mip_push() {
	Add_Filter_Plugin('Filter_Plugin_PostArticle_Succeed','mip_push_Post_Add');//文章编辑成功的接口
	Add_Filter_Plugin('Filter_Plugin_PostPage_Succeed','mip_push_Post_Add');//页面编辑成功的接口
	Add_Filter_Plugin('Filter_Plugin_PostArticle_Core','mip_push_Post_Del_Or_update');//文章提交的核心接口
	Add_Filter_Plugin('Filter_Plugin_PostPage_Core','mip_push_Post_Del_Or_update');//页面编辑的核心接口
	Add_Filter_Plugin('Filter_Plugin_DelArticle_Succeed','mip_push_Post_Del_Or_update');//文章删除成功的接口
	Add_Filter_Plugin('Filter_Plugin_DelPage_Succeed','mip_push_Post_Del_Or_update');//页面删除成功的接口
}

function mip_push_Post_Add($article){
    global $zbp;
	if ($article->Status == 0){
		$urls=$article->Url;
		$api = 'http://data.zz.baidu.com/urls?site='.$zbp->host.'&token='.$zbp->Config('mip_push')->token.'&type=mip';
		$ajax = Network::Create();
		if (!$ajax) {
			throw new Exception('主机没有开启网络功能');
		}
		$ajax->open('POST', $api);
		$ajax->setRequestHeader('Content-Type', 'text/plain');
		$ajax->send($urls);
		$result =json_decode($ajax->responseText);
		if (isset($result->success_mip)) {
			if (isset($result->remain_mip)){
				$zbp->SetHint('good','已成功推送，今天剩余可推送到mip数据的url条数还有：'.$result->remain_mip);
			} else {
				$zbp->SetHint('bad','已成功推送');
			}
		} elseif(isset($result->message)) {
			$zbp->SetHint('bad','推送失败，错误描述：'.$result->message);
		} else {
			$zbp->SetHint('bad','推送失败');
		}
	}
}

function mip_push_Post_Del_Or_update($article){
    global $zbp;
	if ($article->Status == 0 && $article->ID != 0){
    	$api = 'http://mipcache.bdstatic.com/update-ping/c/';
    	$postData = 'key='.$zbp->Config('mip_push')->authkey.'';
    	$url = $api.urlencode(''.$article->Url.'');
    	$ajax = Network::Create();
    	if (!$ajax) {
    		throw new Exception('主机没有开启网络功能');
    	}
    	$ajax->open('POST', $url);
    	$ajax->setRequestHeader('Content-Type', 'text/plain');
    	$ajax->send($postData);
    	$result =json_decode($ajax->responseText);
    	if (isset($result->status)) {
    		if ($result->status == 0){
    			$zbp->SetHint('good','百度Mip-Cache成功');
    		} elseif(isset($result->msg)) {
    			$zbp->SetHint('bad','百度Mip-Cache失败：'.$result->msg);
    		} else {
    			$zbp->SetHint('bad','百度Mip-Cache失败');
    		}
    	} else {
    		$zbp->SetHint('bad','百度Mip-Cache失败');
    	}
	}
}

function InstallPlugin_mip_push(){
	global $zbp;
}

?>