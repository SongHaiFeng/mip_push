# mip_push

#### 项目介绍
ZBLOG免费插件

ID：mip_push

地址：https://app.zblogcn.com/?id=1520

这是一个可以让网站主动向百度搜索推送MIP数据的工具。
百度MIP数据主动推送
在插件后台填写token和authkey后，新建文章/页面、删除文章/页面，都会对应的给百度推送/清除。